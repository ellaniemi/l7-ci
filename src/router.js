const express = require('express');
const router = express();

// router endpoint
router.get("/", (req, res) => res.send("welcome!"));

// router endpoint to add two numbers together. Query parameters are "a" and "b".
router.get("/add", (req, res) => {
    try {
        const sum = parseFloat(req.query.a) + parseFloat(req.query.b);
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;
